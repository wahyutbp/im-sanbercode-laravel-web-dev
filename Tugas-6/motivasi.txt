Halo! Nama saya Wahyu Tri Budi Pangestu biasa dipanggil Wahyu.
Motivasi saya mengikuti bootcamp Laravel Web Dev dari Sanbercode ini adalah kareana saya ingin menjadi seorang programmer dalam ranah web development.
Karena saya tau bahwa framework laravel ini adalah framework php yang paling populer saat ini , jadi saya memilih kelas ini.

Saya belajar di Sanbercode ini dari 0 dan baru pertama kali saya mengikuti bootcamp (sebelumnya sama sekali belum pernah).

Harapan setelah mengikuti bootcamp ini adalah saya bisa merancang web sendiri entah mengunakan nativr maupun framework laravel , dan saya bisa mencari pekerjaan berdasarkan ilmu,pengalaman, project dan sertifikat yang saya dapatkan dari kelas ini.
Terimakasih telah membaca!